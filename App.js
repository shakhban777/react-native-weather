import React from 'react'
import {StatusBar} from 'expo-status-bar'
import {StyleSheet, ScrollView, ImageBackground, Dimensions} from 'react-native'
import {Main} from './components/main'
import {Header} from './components/header'

const backgroundTop = require('./assets/images/top.png')
const backgroundBottom = require('./assets/images/bottom.png')

const {width: viewportWidth} = Dimensions.get('window')

const mobileWidth = 768

const backgroundTopWidth = viewportWidth
const backgroundTopHeight = viewportWidth < mobileWidth
  ? viewportWidth * 1.2
  : viewportWidth * 0.7

const backgroundBottomWidth = viewportWidth < mobileWidth
  ? viewportWidth
  : viewportWidth * 0.7
const backgroundBottomHeight = viewportWidth < mobileWidth
  ? viewportWidth * 1.2
  : viewportWidth * 0.7

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#373af5',
    width: '100%',
    height: '100%',
    minWidth: '100%',
    minHeight: '100%',
  },
  top: {
    position: 'absolute',
    right: 0,
    width: backgroundTopWidth,
    height: backgroundTopHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    position: 'absolute',
    bottom: 0,
    width: backgroundBottomWidth,
    height: backgroundBottomHeight,
    justifyContent: 'center',
    alignItems: 'center',
  }
})

export default function App() {
  return (
    <ScrollView style={styles.container}>
      <StatusBar hidden/>

      <ImageBackground
        source={backgroundTop}
        style={styles.top}/>

      <ImageBackground
        source={backgroundBottom}
        style={styles.bottom}/>

      <Header/>
      <Main/>
    </ScrollView>
  )
}