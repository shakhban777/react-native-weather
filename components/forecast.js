import React from 'react'
import {StyleSheet, Text, View, ScrollView, SafeAreaView} from 'react-native'
import {Placeholder} from './placeholder'
import {SelectCity} from './selectCity'
import {WeatherCard} from './weatherCard'
import {SelectDate} from './selectDate'

export const Forecast = ({
                           title,
                           cities,
                           weeklyWeatherData,
                           changeLocationHandler,
                           blockSelect,
                           historicWeatherData,
                           onChangeDate
                         }) => {
  if (blockSelect === 0) {
    return <WeeklyForecast cities={cities}
                           title={title}
                           weeklyWeatherData={weeklyWeatherData}
                           changeLocationHandler={changeLocationHandler}
                           blockSelect={blockSelect}/>
  } else {
    return <HistoricForecast cities={cities}
                             title={title}
                             historicWeatherData={historicWeatherData}
                             changeLocationHandler={changeLocationHandler}
                             onChangeDate={onChangeDate}
                             blockSelect={blockSelect}/>
  }
}

const WeeklyForecast = ({title, cities, weeklyWeatherData, changeLocationHandler, blockSelect}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <SelectCity cities={cities}
                  style={styles.cities}
                  changeLocationHandler={changeLocationHandler}
                  blockSelect={blockSelect}/>
      {weeklyWeatherData.length
        ? <SafeAreaView style={{flex: 1}}>
          <ScrollView horizontal>
            {
              weeklyWeatherData.map((day) => {
                return <WeatherCard date={day.date}
                                    icon={day.icon}
                                    key={day.id}
                                    temperature={day.temperature}
                                    blockSelect={blockSelect}/>
              })
            }
          </ScrollView>
        </SafeAreaView>
        : <Placeholder/>}
    </View>
  )
}

const HistoricForecast = ({title, cities, historicWeatherData, changeLocationHandler, onChangeDate, blockSelect}) => {
  return (
    <View style={[styles.container, styles.historicBlock]}>
      <Text style={styles.title}>{title}</Text>
      <SelectCity cities={cities}
                  style={styles.cities}
                  changeLocationHandler={changeLocationHandler}
                  blockSelect={blockSelect}/>
      <SelectDate onChangeDate={onChangeDate}/>
      {historicWeatherData.date
        ? <WeatherCard date={historicWeatherData.date}
                       icon={historicWeatherData.icon}
                       temperature={historicWeatherData.temperature}
                       blockSelect={blockSelect}/>
        : <Placeholder/>}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingTop: 32,
    paddingBottom: 60,
    marginHorizontal: 10,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },
  title: {
    fontSize: 32,
    color: '#2C2D76',
    marginBottom: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  cities: {
    marginBottom: 30
  },
  historicBlock: {
    marginTop: 10,
    marginBottom: 50
  }
})