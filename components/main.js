import React, {useState, useEffect} from 'react'
import {Forecast} from './forecast'
import {WeatherApiService} from '../api/api'
import {StyleSheet, View} from 'react-native'

export const Main = () => {
  const [cities] = useState([
    {label: 'Самара', value: '53.195873,50.100193'},
    {label: 'Тольятти', value: '53.507836,49.420393'},
    {label: 'Саратов', value: '51.533557,46.034257'},
    {label: 'Казань', value: '55.796127,49.106405'},
    {label: 'Краснодар', value: '45.035470,38.975313'}
  ])

  const [location, setLocation] = useState([{coords: ''}, {coords: ''}])
  const [weeklyWeatherData, setWeeklyWeatherData] = useState([])
  const [historicWeatherData, setHistoricWeatherData] = useState({date: '', icon: '', temperature: ''})
  const [date, setDate] = useState(null)

  const locationForSevenDaysWeather = location[0]
  const locationForHistoricWeather = location[1]

  useEffect(() => {
    if (locationForSevenDaysWeather.length) {
      const weather = new WeatherApiService()
      weather.getWeatherForSevenDays(locationForSevenDaysWeather)
        .then(response => {
          setWeeklyWeatherData(response)
        })
    }
  }, [locationForSevenDaysWeather])

  useEffect(() => {
    if (locationForHistoricWeather.length && date) {
      const weatherService = new WeatherApiService()
      weatherService.getWeatherForHistoricDate(locationForHistoricWeather, date)
        .then(historicForecast => {
          setHistoricWeatherData(historicForecast)
        })
    }
  }, [locationForHistoricWeather, date])

  const changeLocationHandler = (coords, blockSelect) => {
    if (blockSelect === 0) {
      setLocation(prevState => {
        return [prevState[0] = coords, prevState[1]]
      })
    } else {
      setLocation(prevState => {
        return [prevState[0], prevState[1] = coords]
      })
    }
  }

  return (
    <View style={styles.container}>
      <Forecast title={'7 Days Forecast'}
                cities={cities}
                weeklyWeatherData={weeklyWeatherData}
                changeLocationHandler={changeLocationHandler}
                blockSelect={0}/>
      <Forecast title={'Forecast for a Date in the Past'}
                cities={cities}
                historicWeatherData={historicWeatherData}
                changeLocationHandler={changeLocationHandler}
                onChangeDate={(date) => setDate(date)}
                blockSelect={1}/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  }
})
