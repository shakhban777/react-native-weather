import React from 'react'
import {StyleSheet, Text, View} from 'react-native'

export const Header = () => {
  return (
    <View style={styles.header}>
      <View style={styles.container}>
        <Text style={styles.text}>Weather</Text>
        <Text style={styles.textRight}>forecast</Text>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  header: {
    flex: 1,
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 32,
    marginBottom: 24,
    width: 243,
  },
  text: {
    flex: 1,
    color: '#fff',
    fontSize: 32,
    fontWeight: 'bold',
  },
  textRight: {
    flex: 1,
    color: '#fff',
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'right',
  },
})
