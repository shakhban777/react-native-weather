import React, {useState} from 'react'
import {StyleSheet} from 'react-native'
import DatePicker from 'react-native-datepicker'

export const SelectDate = ({onChangeDate}) => {

  const [date, setDate] = useState(null)

  const onDateChangeHandler = (newDate) => {
    setDate(newDate)
    const parts = newDate.split('-')
    const myDate = new Date(parts[2], parts[1] - 1, parts[0])
    const parsedDate = myDate.getTime() / 1000
    onChangeDate(parsedDate)
  }

  return (
    <DatePicker
      style={styles.date}
      date={date}
      mode="date"
      placeholder="Select date"
      format="DD-MM-YYYY"
      minDate={new Date(Date.now() - 86400000 * 5)}
      maxDate={new Date()}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      showIcon={false}
      customStyles={{
        dateIcon: {
          position: 'absolute',
          left: 0,
          marginLeft: 0,
        },
        dateInput: {
          position: 'absolute',
          top: -10,
          left: 0,
          borderWidth: 0,
          fontSize: 16
        },
      }}
      onDateChange={(newDate) => onDateChangeHandler(newDate)}
    />
  )
}

const styles = StyleSheet.create({
  date: {
    marginTop: 32,
    marginLeft: 24,
    marginRight: 24,
    width: '100%',
    height: 48,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: 'rgba(128, 131, 164, 0.2)',
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 8,
  },
})