import React from 'react'
import {Image, StyleSheet, Text, View} from 'react-native'
import placeholder from '../assets/images/placeholder.png'

export const Placeholder = () => {
  return (
    <View style={styles.container}>
      <Image source={placeholder}/>
      <Text style={styles.placeholderText}>Fill in all the fields and the weather will be displayed</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginVertical: 25
  },
  placeholderText: {
    fontSize: 16,
    textAlign: 'center',
    maxWidth: 240,
    lineHeight: 24,
    color: '#8083A4',
    fontWeight: 'bold'
  }
})