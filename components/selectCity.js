import React, {useState} from 'react'
import {StyleSheet} from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker'

export const SelectCity = ({cities, changeLocationHandler, blockSelect}) => {
  const [open, setOpen] = useState(false)
  const [value, setValue] = useState(null)

  return (
    <DropDownPicker
      style={styles.picker}
      placeholder='Select city'
      open={open}
      value={value}
      items={cities}
      setOpen={setOpen}
      setValue={setValue}
      onChangeValue={(value) => changeLocationHandler(value, blockSelect)}
    />
  )
}

const styles = StyleSheet.create({
  picker: {
    borderColor: 'rgba(128, 131, 164, 0.2)',
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
    borderWidth: 2,
    borderStyle: 'solid',
    borderRadius: 8,
  }
})