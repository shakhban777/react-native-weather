import React from 'react'
import {Image, StyleSheet, Text, View} from 'react-native'

export const WeatherCard = ({date, icon, temperature, blockSelect}) => {
  if (blockSelect === 0) {
    return <WeeklyWeatherCard date={date}
                              icon={icon}
                              temperature={temperature}/>
  } else if (blockSelect === 1) {
    return <HistoricWeatherCard date={date}
                                icon={icon}
                                temperature={temperature}/>
  }
}

const WeeklyWeatherCard = ({date, icon, temperature}) => {
  return (
    <View style={styles.card}>
      <Text style={styles.date}>{date}</Text>
      <Image style={styles.image} source={{uri: icon}}/>
      <Text style={styles.temperature}>{temperature}</Text>
    </View>
  )
}

const HistoricWeatherCard = ({date, icon, temperature}) => {
  return (
    <View style={[styles.card, styles.historicCard]}>
      <Text style={styles.date}>{date}</Text>
      <Image style={styles.image} source={{uri: icon}}/>
      <Text style={styles.temperature}>{temperature}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    alignItems: 'center',
    height: 238,
    width: 174,
    backgroundColor: '#373af5',
    padding: 20,
    borderRadius: 8,
    shadowColor: 'rgba(5, 6, 114, 0.2)',
    shadowOpacity: 1,
    shadowRadius: 20,
    shadowOffset: {
      width: 14, height: 14
    },
    marginRight: 16,
    marginTop: 30,
  },
  date: {
    fontSize: 16,
    lineHeight: 24,
    fontWeight: 'bold',
    color: '#fff',
    width: '100%',
  },
  image: {
    height: 120,
    width: 120,
  },
  temperature: {
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
    width: '100%',
    textAlign: 'right',
  },
  historicCard: {
    width: '100%',
    marginRight: 0
  }
})
