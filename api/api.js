class WeatherApiService {
   _apiKey = '9f794cbee16169a67f1379107a9a4b6e'
   _baseURL = 'https://api.openweathermap.org/data/2.5/onecall'

   symbolOfWeather = (temperature) => {
      if (temperature - 273.15 > 0) {
         return '+'
      } else if (temperature - 273.15 < 0) {
         return '-'
      } else {
         return ''
      }
   }

   fetchFromServer = async (urlToFetch) => {
      const dataFromServer = await fetch(urlToFetch)

      if (!dataFromServer.ok) {
         throw new Error(`Could not fetch ${dataFromServer}, received ${dataFromServer.status}`)
      }

      return await dataFromServer.json()
   }

	parseDate = (date) => {
		const dateParts = date.split(' ')
		return `${dateParts[2]} ${dateParts[1]} ${dateParts[4]}`
	}

   getWeatherForSevenDays = async (coords) => {
		const coordsArray = coords.split(',')
		const [lat, lon] = coordsArray

      const weatherForSevenDaysUrl = `${this._baseURL}?lat=${lat}&lon=${lon}&&exclude=current,minutely,hourly,alerts&appid=${
         this._apiKey
      }`

      const weatherDataForSevenDaysJson = await this.fetchFromServer(weatherForSevenDaysUrl)
      const weatherForSevenDaysArray = weatherDataForSevenDaysJson.daily
      return weatherForSevenDaysArray.map((day) => {
         const symbolOfWeather = this.symbolOfWeather(day.temp.day)
			const convertedDate = new Date(day.dt * 1000)
				.toLocaleString("en", {year: 'numeric', month: 'short', day: "numeric"})
				.split(', ')
				.join(' ')
			const date = this.parseDate(convertedDate)
         return {
            id: day.dt,
            icon: `https://openweathermap.org/img/wn/${day.weather.find(el => el.icon)?.icon}@2x.png`,
            date: date,
            temperature: symbolOfWeather + Math.round(day.temp.day - 273.15).toString() + '°'
         }
      })
   }

   getWeatherForHistoricDate = async (coords, date) => {
		const coordsArray = coords.split(',')
		const [lat, lon] = coordsArray

      const weatherForHistoricDateUrlToFetch = `${this._baseURL}/timemachine?lat=${lat}&lon=${lon}&dt=${date}&appid=${this._apiKey}`
      try {
         const weatherForHistoricDateJson = await this.fetchFromServer(weatherForHistoricDateUrlToFetch)
         const weatherOfHistoricDay = weatherForHistoricDateJson.current
         const symbolOfWeather = this.symbolOfWeather(weatherOfHistoricDay.temp)
			const convertedDate = new Date(weatherOfHistoricDay.dt * 1000).toLocaleString("en",
				{year: 'numeric', month: 'short', day: "numeric"}).split(', ').join(' ')
			const date = this.parseDate(convertedDate)

         return {
            icon: `https://openweathermap.org/img/wn/${weatherOfHistoricDay.weather[0].icon}@2x.png`,
            date: date,
            temperature: `${symbolOfWeather}${Math.round(weatherOfHistoricDay.temp - 273.15).toString()}°`
         }
      } catch (error) {
         console.error(error)
         return {
            icon: '',
            date: '',
            temperature: ''
         }
      }
   }
}

export { WeatherApiService }